﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Распознавание_образов___4 {
	/// <summary>
	/// Логика взаимодействия для MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {
		List<TextBox> vectM = new List<TextBox>();
		List<TextBox> matrV = new List<TextBox>();
		
		List<int>	numList;
		List<int>	numVectCount;
		List<float>	pList;
		List<float[]>  matVectList; //Мат. ожидаине
		List<float[,]> corrMatrList; //корелляция
		int numOfShowClass = 1, numClassCount = 1;

		public MainWindow() {
			InitializeComponent();

			ClassNum.Text = "Кол-во:  " + numClassCount.ToString();
			mBox.Text = "Отображается:  " + (numOfShowClass).ToString();

			getNewMV();

			Start_Button_Click(this, null);

		}
		private void getNewMV() {
			numList      = new List<int>();
			numVectCount = new List<int>();
			pList        = new List<float>();
			matVectList  = new List<float[]>();
			corrMatrList = new List<float[,]>();

			int tempVecCount = 2;

			for(int k=0; k < numClassCount; k++) {
				float[]  tempMatV     = new float[tempVecCount];
				float[,] tempCorrMatr = new float[tempVecCount, tempVecCount];

				for(int i=0; i < tempVecCount; i++)
					tempMatV[i] = 200;

				for(int i=0; i < tempVecCount; i++)
					for(int j=0; j < tempVecCount; j++)
						tempCorrMatr[i, j] = (i == j) ? 50 : 0;
				numList.Add(300);
				numVectCount.Add(tempVecCount);
				pList.Add(0.45f);
				matVectList.Add(tempMatV);
				corrMatrList.Add(tempCorrMatr);
			}
			drowMatr();
		}
		private void getNewMV(int tempVecCount = 2) {
			for(int k=0; k < numClassCount; k++) {
				float[]  tempMatV     = new float[tempVecCount];
				float[,] tempCorrMatr = new float[tempVecCount, tempVecCount];

				for(int i=0; i < tempVecCount; i++)
					tempMatV[i] = 200;

				for(int i=0; i < tempVecCount; i++)
					for(int j=0; j < tempVecCount; j++)
						tempCorrMatr[i, j] = (i == j) ? 50 : 0;
				numList     [numOfShowClass - 1] = 300;
				numVectCount[numOfShowClass - 1] = tempVecCount;
				pList       [numOfShowClass - 1] = 1;
				matVectList [numOfShowClass - 1] = tempMatV;
				corrMatrList[numOfShowClass - 1] = tempCorrMatr;
			}
			drowMatr();
		}
		private void drowMatr() {
			int			  tempNum =      numList[numOfShowClass - 1];
			int		 tempVecCount = numVectCount[numOfShowClass - 1];
			float         tempVer =        pList[numOfShowClass - 1];
			float[]      tempMatV =  matVectList[numOfShowClass - 1];
			float[,] tempCorrMatr = corrMatrList[numOfShowClass - 1];

			VecSize.Text = tempVecCount.ToString();
			elemNum.Text = tempNum.ToString();
			Ver.Text     = tempVer.ToString();

			vectM.Clear();
			matrV.Clear();
			matrGrid.Children.Clear();
			corGrid.Children.Clear();

			double x=10, y = 10;
			for(int i=0; i < tempVecCount; i++) {
				TextBox T = new TextBox();
				T.Text = tempMatV[i].ToString();
				matrGrid.Children.Add(T);
				T.VerticalAlignment = VerticalAlignment.Top; T.HorizontalAlignment = HorizontalAlignment.Left;
				T.Width = 60; T.Height = 25;
				T.TextAlignment = TextAlignment.Center;
				T.Margin = new Thickness(x, y, 0, 0);
				vectM.Add(T);
				x += 65;
			}

			x = 10; y = 10;
			for(int i=0; i < tempVecCount; i++) {

				for(int j=0; j <= i; j++) {
					TextBox T = new TextBox();
					T.Text = tempCorrMatr[i, j].ToString();
					corGrid.Children.Add(T);
					T.VerticalAlignment = VerticalAlignment.Top; T.HorizontalAlignment = HorizontalAlignment.Left;

					T.Width = 60; T.Height = 25;

					T.TextAlignment = TextAlignment.Center;
					T.Margin = new Thickness(x, y, 0, 0);
					matrV.Add(T);
					x += 65;
				}
				x = 10;
				y += 30;
			}
			mBox.Text = "Отображается:  " + (numOfShowClass).ToString();
		}
		private void drowMatr(int tempVecCount, float tempVer, float[] tempMatV, float[,] tempCorrMatr) {
			matrGrid2.Children.Clear();
			corGrid2.Children.Clear();

			Ver2.Text     = tempVer.ToString();

			double x = 10, y = 10;
			for(int i = 0; i < tempVecCount; i++) {
				TextBlock T = new TextBlock();
				T.Text = Math.Round(tempMatV[i],2).ToString();
				matrGrid2.Children.Add(T);
				T.VerticalAlignment = VerticalAlignment.Top; T.HorizontalAlignment = HorizontalAlignment.Left;
				T.Width = 60; T.Height = 25;
				T.TextAlignment = TextAlignment.Center;
				T.Margin = new Thickness(x, y, 0, 0);
				x += 65;
			}

			x = 10; y = 10;
			for(int i = 0; i < tempVecCount; i++) {
				for(int j = 0; j <= i; j++) {
					TextBlock T = new TextBlock();
					T.Text = Math.Round(tempCorrMatr[i, j],2).ToString();
					corGrid2.Children.Add(T);
					T.VerticalAlignment = VerticalAlignment.Top; T.HorizontalAlignment = HorizontalAlignment.Left;
					T.Width = 60; T.Height = 25;
					T.TextAlignment = TextAlignment.Center;
					T.Margin = new Thickness(x, y, 0, 0);
					x += 65;
				}
				x = 10;
				y += 30;
			}
			mBox.Text = "Отображается:  " + (numOfShowClass).ToString();
		}
		private bool saveData() {
			int	  tempNum;
			int   tempVecCount;
			float tempVer;
			float[]  tempMatV;
			float[,] tempCorrMatr;
			
			if(!Int32.TryParse(elemNum.Text, out tempNum))		 { MessageBox.Show("Введите корректное колличество элементов"); return false; }
			if(!Int32.TryParse(VecSize.Text, out tempVecCount))  { MessageBox.Show("Введите корректную размерность вектора");   return false; }
			if(!float.TryParse(Ver.Text,	 out tempVer))		 { MessageBox.Show("Введите корректную вероятность");		    return false; }

			    tempMatV = new float[tempVecCount];
			tempCorrMatr = new float[tempVecCount, tempVecCount];

			for(int i=0; i < tempVecCount; i++)
				if(!float.TryParse(vectM[i].Text, out tempMatV[i])) { MessageBox.Show("Введите корректное значение"); return false; }

			int k=0;
			for(int i=0; i < tempVecCount; i++)
				for(int j=0; j <= i; j++)
					if(!float.TryParse(matrV[k++].Text, out tempCorrMatr[i, j])) { MessageBox.Show("Введите корректное значение"); return false; }

			     numList[numOfShowClass - 1] = tempNum;
			numVectCount[numOfShowClass - 1] = tempVecCount;
			       pList[numOfShowClass - 1] = tempVer;
			 matVectList[numOfShowClass - 1] = tempMatV;
			corrMatrList[numOfShowClass - 1] = tempCorrMatr;

			return true;
		}
		private void Start_Button_Click(object sender, RoutedEventArgs e) {
			if(!saveData()) return;

			Random Rnd = new Random();
			List<List<Point>> pointLists = new List<List<Point>>();

			int			  tempNum;
			int		 tempVecCount;
			float         tempVer;
			float[]      tempMatV;
			float[,] tempCorrMatr;

			for(int k = 0; k < numClassCount; k++) {
				tempNum =      numList[k];
				tempVecCount = numVectCount[k];
				tempVer =        pList[k];
				tempMatV =  matVectList[k];
				tempCorrMatr = corrMatrList[k];

				List<Point> pointList = new List<Point>();

				// генерация выборки
				float [,] A=new float[tempVecCount, tempVecCount];
				A[0, 0] = tempCorrMatr[0, 0];
				for(int i = 0; i < tempVecCount; i++) {
					A[i, 0] = tempCorrMatr[i, 0];
					for(int j = 1; j < i; j++) {
						float sum = 0;
						for(int m = 0; m < i; m++)
							sum += A[i, m] * A[j, m];
						A[i, j] = (tempCorrMatr[i, j] - sum) / A[j, j];
					}
					float ssum = 0;
					for(int m = 0; m < i; m++)
						ssum += A[i, m] * A[i, m];
					A[i, i] = (float)Math.Sqrt(1 - ssum);
				}


				int Count = (int)(tempNum/* * tempVer*/);
				float[,] Points = new float[Count, tempVecCount];
				pointList.Add(new Point(tempMatV[0], tempMatV[1]));
				for(int i = 0; i < Count; i++) {
					float[] X = new float[tempVecCount];	//генерация некореллированной выборки
					for(int j = 0; j < tempVecCount; j++) {
						float sum = 0;
						for(int p = 0; p < 12; p++)
							sum += (float)Rnd.NextDouble();
						X[j] = sum - 6;
					}
					for(int j = 0; j < tempVecCount; j++) {
						Points[i, j] = 0;
						for(int  p = 0; p <= j; p++)
							Points[i, j] += X[p] * A[j, p];
						Points[i, j] = tempCorrMatr[j, j] * Points[i, j] + tempMatV[j];

					}
					float genVer = (float) Rnd.NextDouble();
					if(genVer <= tempVer)
						pointList.Add(new Point(Points[i, 0], Points[i, 1]));

				}
				pointLists.Add(pointList);


				if(k == numOfShowClass - 1) {
					float NewProb = (float) pointLists[numOfShowClass - 1].Count / tempNum;
					float[] NewMat = new float[tempVecCount];

					for(int j = 0; j < tempVecCount; j++) {
						NewMat[j] = 0;
						for(int i = 0; i < Count; i++)
							NewMat[j] += Points[i, j];
						NewMat[j] /= Count;
					}


					float[,] NewCorr = new float[tempVecCount, tempVecCount];
					for(int i = 0; i < tempVecCount; i++) {
						float sum = 0;
						for(int j = 0; j < Count; j++)
							sum += Points[j, i] * Points[j, i];
						NewCorr[i, i] = (float) Math.Sqrt(sum / Count - NewMat[i] * NewMat[i]);
						for(int j = 0; j < i; j++) {
							sum = 0;
							for(int kk = 0; kk < Count; kk++)
								sum += (Points[kk, i] - NewMat[i]) * (Points[kk, j] - NewMat[j]);
							NewCorr[i, j] = sum / Count / NewCorr[i, i] / NewCorr[j, j];
						}
					}
					drowMatr(tempVecCount, NewProb, NewMat, NewCorr);
				}
			}
			wPlot.setData(pointLists);			
		}
		private void pButP_Click(object sender, RoutedEventArgs e) {
			if(numClassCount > numOfShowClass)
				if(saveData()) {
					numOfShowClass++;
					drowMatr();
				}
		}
		private void pButM_Click(object sender, RoutedEventArgs e) {
			if(numOfShowClass > 1) 
				if(saveData()) {
					numOfShowClass--;
					drowMatr();
				}
		}
		private void ClassNumP_Click(object sender, RoutedEventArgs e) {
			if(numClassCount < 10) {
				numClassCount++;
				ClassNum.Text = "Кол-во:  " + numClassCount.ToString();
				getNewMV();
			}
		}
		private void ClassNumM_Click(object sender, RoutedEventArgs e) {
			if(numClassCount > 1) {
				numClassCount--;
				ClassNum.Text = "Кол-во:  " + numClassCount.ToString();
				getNewMV();
			}
		}
		private void VecSizeP_Click(object sender, RoutedEventArgs e) {
			int tempVecCount;
			Int32.TryParse(VecSize.Text, out tempVecCount);

			if(tempVecCount < 5) {
				tempVecCount++;
				VecSize.Text = tempVecCount.ToString();
				getNewMV(tempVecCount);
			}
		}
		private void VecSizeM_Click(object sender, RoutedEventArgs e) {
			int tempVecCount;
			Int32.TryParse(VecSize.Text, out tempVecCount);

			if(tempVecCount > 2) {
				tempVecCount--;
				VecSize.Text = tempVecCount.ToString();
				getNewMV(tempVecCount);
			}
		}
		private void Graphs_Button_Click(object sender, RoutedEventArgs e) {
			Start_Button_Click(this, null);
			Graphs openWnew = new Graphs(numList[numOfShowClass - 1], numVectCount[numOfShowClass - 1], pList[numOfShowClass - 1], matVectList[numOfShowClass - 1], corrMatrList[numOfShowClass - 1]);
			openWnew.IsEnabled = true;
			openWnew.ShowInTaskbar = true; // Не показывать в панели задач
			openWnew.ShowDialog();
		}
	}
}
