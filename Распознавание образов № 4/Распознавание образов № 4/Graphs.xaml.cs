﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Распознавание_образов___4 {
	public partial class Graphs : Window {
		int selectionSize;
		int vectorSize;
		float inputProb;
		float[] inputMatVector;
		float[,] inputCorrMatr;


		Random Rnd = new Random();
		public Graphs(int tSelectionSize, int tVectorSize, float tInputProb, float[] tInputMatVector, float[,] tInputCorrMatr) {
			InitializeComponent();

			selectionSize = tSelectionSize;
			vectorSize = tVectorSize;
			inputProb = tInputProb;
			inputMatVector = tInputMatVector;
			inputCorrMatr = tInputCorrMatr;

			Button_Click(this,null);
		}
		private void Button_Click(object sender, RoutedEventArgs e) {
		//	List<Point> pointListVer = new List<Point>();
		//	List<Point> pointListMat = new List<Point>();
		//	List<Point> pointListVect = new List<Point>();

			int min, max, step;
			if(!Int32.TryParse(minP.Text, out min)) {
				MessageBox.Show("Введите корректное колличество элементов"); return;
			}
			if(!Int32.TryParse(maxP.Text, out max)) {
				MessageBox.Show("Введите корректное колличество элементов"); return;
			}
			if(!Int32.TryParse(stepP.Text, out step)) {
				MessageBox.Show("Введите корректное колличество элементов"); return;
			}



		//	for(long k = min; k <= max; k += step) {
		//		int count = 0;
		//		for(int i = 0; i < selectionSize; i++) {
		//			float R = (float) Rnd.NextDouble();
		//			if(R <= inputProb)
		//				count++;
		//		}
		//		cGenerator G = new cGenerator(count, vectorSize, inputProb, inputMatVector, inputCorrMatr, Rnd);

		//		cAnaliz C = new cAnaliz(count, vectorSize, G.getPoints());

		//		pointListVer.Add(new Point(k, (float) count / selectionSize));
		//		pointListMat.Add(new Point(k, C.getMat()));
		//		pointListVect.Add(new Point(k, C.getCorr()));

		//	}
		//	wPlot1.setData(inputProb, pointListVer);
		//	wPlot2.setData(inputMatVector[0], pointListMat);
		//	wPlot3.setData(inputCorrMatr[0, 0], pointListVect);
			
			
			List<List<Point>> elements = new List<List<Point>>();


			for(int k = min; k <= max; k += step) {
				// 0 - realMat
				// 1 - RealProbability
				// 2 - theor Mat
				// 3 - theor Prob
				// 4 - real dispersion
				// 5 - teor dispersion
				List<Point> series = new List<Point>();

				// генерация выборки
				int count = 0;
				for(int i = 0; i < k; i++)
					if(Rnd.NextDouble() <= inputProb)
						count++;

				float[,] points = ElementsGeneration(inputCorrMatr, inputMatVector, Rnd, vectorSize, inputProb, k, count);

				//Расчет реальных значений
				float outputProb;
				float[] ouputMatVector;
				float[,] outputCorrMatr;
				CalculateRealValues(vectorSize, k, count, points, out outputProb, out ouputMatVector, out outputCorrMatr);

				//add
				series.Add(new Point(k, outputProb));
				series.Add(new Point(k, ouputMatVector[0]));
				series.Add(new Point(k, outputCorrMatr[0, 0]));


				series.Add(new Point(k, inputProb));
				series.Add(new Point(k, inputMatVector[0]));
				series.Add(new Point(k, inputCorrMatr[0, 0]));


				elements.Add(series);
			}
			wPlot1.setData(0, 3, elements);
			wPlot2.setData(1, 4, elements);
			wPlot3.setData(2, 5, elements);
		}

		private float[,] ElementsGeneration(float[,] correlation, float[] mat, Random rand, int vectorDimensions, float probability, int vectorSize, int count) {
			float[,] samples = new float[vectorDimensions, vectorDimensions];
			samples[0, 0] = correlation[0, 0];
			for(int i = 0; i < vectorDimensions; i++) {
				samples[i, 0] = correlation[i, 0];
				for(int j = 1; j < i; j++) {
					float sum = 0;
					for(int m = 0; m < i; m++)
						sum += samples[i, m] * samples[j, m];
					samples[i, j] = (correlation[i, j] - sum) / samples[j, j];
				}
				float ssum = 0;
				for(int m = 0; m < i; m++)
					ssum += samples[i, m] * samples[i, m];
				samples[i, i] = (float) Math.Sqrt(1 - ssum);

			}
			
			float[,] points = new float[count, vectorDimensions];
			for(int i = 0; i < count; i++) {
				//генерация некореллированной выборки
				float[] nonCorrelationSamples = new float[vectorDimensions];
				for(int j = 0; j < vectorDimensions; j++) {
					float sum = 0;
					for(int k = 0; k < 12; k++)
						sum += (float) rand.NextDouble();
					nonCorrelationSamples[j] = sum - 6;
				}
				for(int j = 0; j < vectorDimensions; j++) {
					points[i, j] = 0;
					for(int k = 0; k <= j; k++)
						points[i, j] += nonCorrelationSamples[k] * samples[j, k];
					points[i, j] = correlation[j, j] * points[i, j] + mat[j];

				}
			}
			return points;
		}

		private static void CalculateRealValues(int vectorDimensions, int vectorSize, int count, float[,] points, out float realProbability, out float[] realMat, out float[,] realCorrelation) {
			realProbability = (float) count / vectorSize;
			realMat = new float[vectorDimensions];
			for(int j = 0; j < vectorDimensions; j++) {
				realMat[j] = 0;
				for(int i2 = 0; i2 < count; i2++)
					realMat[j] += points[i2, j];
				realMat[j] /= count;
			}
			realCorrelation = new float[vectorDimensions, vectorDimensions];
			for(int i2 = 0; i2 < vectorDimensions; i2++) {
				float sum = 0;
				for(int j = 0; j < count; j++)
					sum += points[j, i2] * points[j, i2];
				realCorrelation[i2, i2] = (float) Math.Sqrt(sum / count - realMat[i2] * realMat[i2]);
				for(int j = 0; j < i2; j++) {
					sum = 0;
					for(int k = 0; k < count; k++)
						sum += (points[k, i2] - realMat[i2]) * (points[k, j] - realMat[j]);
					realCorrelation[i2, j] = sum / count / realCorrelation[i2, i2] / realCorrelation[j, j];
				}
			}
		}

	}
}
