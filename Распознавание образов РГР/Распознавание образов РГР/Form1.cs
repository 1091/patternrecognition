﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Распознавание_образов_РГР {
    public partial class Form1:Form {
        string driveLetter="e:/";
        float threshold = 50, areaS = 1000;
        int vBlack=0, vWhite=1;
        bool flagSave1=false, flagSave2=false;

        public Form1() {
            InitializeComponent();
            comboBox1.Items.Add("z1");
            comboBox1.Items.Add("z2");
            comboBox1.Items.Add("z3");
            comboBox1.Items.Add("z4");
            comboBox1.Items.Add("z5");
            comboBox1.Items.Add("z6");
            comboBox1.Items.Add("z8");
            comboBox1.Items.Add("z9");
            comboBox1.Items.Add("z10");
            comboBox1.SelectedIndex = 0;
        }

        private void buttonStartToBinary_Click(object sender, EventArgs e) {
            try {
                int picWith, picHeight;
                int[,] color = getMatr(out picWith, out picHeight);

                pictureBox1.Image = pictureBox1.Image = printMatrToBmp(color, picWith, picHeight);
            } catch(ArgumentException) {
                MessageBox.Show("Произошла ошибка!!!");
            }
        }

        private void buttonStartAnalysis_Click(object sender, EventArgs e) {
            try {
                int picWith, picHeight;
                int[,] color = getMatr(out picWith, out picHeight);
                // Цикл по пикселям изображения
                int km = 0, kn = 0; // Они нам еще пригодятся
                int cur = 1; // Переменная для подсчета объектов
                int A, B, C;

                Dictionary<int, int> eqv = new Dictionary<int, int>();
                for(int j = 0; j < picHeight; j++) {
                    for(int i = 0; i < picWith; i++) {
                        kn = j - 1;
                        if(kn < 0) B = 0;
                        else B = color[kn, i]; // Смотри рисунок 3 в статье

                        km = i - 1;
                        if(km < 0) C = 0;
                        else C = color[j, km]; // Смотри рисунок 3 в статье

                        A = color[j, i]; // Смотри рисунок 3 в статье

                        if(A != 0) // Если в текущем пикселе пусто - то ничего не делаем
                            if(B == 0 && C == 0) { // Если вокруг нашего пикселя пусто а он не пуст - то это повод подумать о новом объекте
                                color[j, i] = ++cur;
                                eqv.Add(cur, cur);
                            } else
                                if(B != 0 && C == 0)
                                    color[j, i] = B;
                                else
                                    if(B == 0 && C != 0)
                                        color[j, i] = C;
                                    else
                                        if(B != 0 && C != 0)
                                            if(B == C)
                                                color[j, i] = B;
                                            else {
                                                color[j, i] = B;
                                                eqv[C] = B;
                                            }
                    }

                    for(int i = picWith - 1; i >= 0; i--) {
                        kn = j - 1;
                        if(kn < 0) B = 0;
                        else B = color[kn, i]; // Смотри рисунок 3 в статье

                        km = i + 1;
                        if(km >= picWith) C = 0;
                        else C = color[j, km]; // Смотри рисунок 3 в статье

                        A = color[j, i]; // Смотри рисунок 3 в статье

                        if(A != 0) // Если в текущем пикселе пусто - то ничего не делаем
                            if(B != 0 && C == 0)
                                color[j, i] = B;
                            else
                                if(B == 0 && C != 0)
                                    color[j, i] = C;
                                else
                                    if(B != 0 && C != 0)
                                        if(B == C)
                                            color[j, i] = B;
                                        else {
                                            color[j, i] = B;
                                            eqv[C] = B;
                                        }
                    }
                }

                Dictionary<int, int> sum = new Dictionary<int, int>();
                for(int x = 0; x < picWith; x++)
                    for(int y = 0; y < picHeight; y++) {
                        int mark = color[y, x];
                        if(mark != 0) {
                            if(eqv[mark] != mark) {
                                color[y, x] = eqv[mark];
                                mark = color[y, x];
                            }
                            if(sum.ContainsKey(mark))
                                sum[mark]++;
                            else
                                sum.Add(mark, 1);
                        }


                    }
                pictureBox1.Image = printMatrToBmp(color, picWith, picHeight, sum);
            } catch(ArgumentException) {
                MessageBox.Show("Произошла ошибка!!!");
            }
        }
       
		int[,] getMatr(out int picWith, out int picHeight) {
			string driveLetterA  = textBox1.Text + ":/";
            string file = driveLetterA + (string)comboBox1.SelectedItem + ".bmp";
            Bitmap bmp = new Bitmap(file); //  - адрес изображения
            picWith = bmp.Width; picHeight = bmp.Height;

            int[,] color = new int[picHeight, picWith];
            for(int x = 0; x < picWith; x++)
                for(int y = 0; y < picHeight; y++)
                    color[y, x] = (bmp.GetPixel(x, y).GetBrightness() > threshold / 100) ? vBlack : vWhite;
            return color;
        }

        Bitmap printMatrToBmp(int[,] matrC, int With, int Height, Dictionary<int, int> sumDict = null) {
            Bitmap bmp = new Bitmap(
                    With,
                    Height,
                    System.Drawing.Imaging.PixelFormat.Format32bppArgb
                );

            //using(Graphics gfx = Graphics.FromImage(newBmp)) {
            //	gfx.DrawImage(bmp, 0, 0);
            //}

            for(int x = 0; x < With; x++)
                for(int y = 0; y < Height; y++) {
                    int mark = matrC[y, x];
                    switch(mark) {
                        case 0:
                            bmp.SetPixel(x, y, Color.White);
                            break;
                        case 1:
							bmp.SetPixel(x, y, Color.Black);
                            break;
                        default:
                            if(sumDict[mark] > areaS)
                                bmp.SetPixel(x, y, Color.Red);
                            else
								bmp.SetPixel(x, y, Color.Black);
                            break;
                    }
                }

			string driveLetterA  = textBox1.Text + ":/";

            if(flagSave1) {
                string file = driveLetterA + (string)comboBox1.SelectedItem + "_after_change.bmp";
                bmp.Save(file);
            }


            if(flagSave2) {
                string file = driveLetterA + (string)comboBox1.SelectedItem + "_work_matr.txt";
                StreamWriter steamWriter = new StreamWriter(file);
                for(int y = 0; y < Height; y++) {
                    for(int x = 0; x < With; x++) {
                        string tempS = matrC[y, x].ToString();
                        steamWriter.Write(String.Format(" {0,4} ", tempS));
                    }
                    steamWriter.WriteLine();
                }
                steamWriter.Close();
            }
            return bmp;
        }

        private void buttonUndo_Click(object sender, EventArgs e) {
			try {
				string driveLetterA  = textBox1.Text + ":/";
				string file = driveLetterA + (string)comboBox1.SelectedItem + ".bmp";
				Bitmap bmp = new Bitmap(file); //  - адрес изображения
				pictureBox1.Image = bmp;
			} catch(ArgumentException) {
				MessageBox.Show("Произошла ошибка!!!");
			}

        }
        private void numSize_ValueChanged(object sender, EventArgs e) {
            threshold = (float)numSize.Value;
        }
        private void areaSize_ValueChanged(object sender, EventArgs e) {
            areaS = (float)areaSize.Value;
        }
        private void checkBox1_CheckedChanged(object sender, EventArgs e) {
            int temp = vWhite;
            vWhite = vBlack;
            vBlack = temp;
        }
        private void checkBox2_CheckedChanged(object sender, EventArgs e) {
            flagSave1 = !flagSave1;
        }
        private void checkBox3_CheckedChanged(object sender, EventArgs e) {
            flagSave2 = !flagSave2;
        }
    }
}
