﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;


namespace Распознавание_образов___1 {
	public partial class cClock {

		Grid grid;

		double x1 = 10, y1 = 10;
		double vRad = 180;

		Ellipse bigEl1, smallEl1;

		Line hoursL1, minetsL1, secondsL1;
		List<Line>   bigLines1 = new List<Line>();
		List<Line> smallLines1 = new List<Line>();

		SolidColorBrush vBrush = Brushes.White;

		public cClock(Grid G) {
			grid = G;

			bigEl1 = new Ellipse(); smallEl1 = new Ellipse();
			grid.Children.Add(bigEl1); grid.Children.Add(smallEl1);

			secondsL1 = new Line(); minetsL1 = new Line(); hoursL1 = new Line();
			grid.Children.Add(secondsL1); grid.Children.Add(minetsL1); grid.Children.Add(hoursL1);

			for(int i = 1; i <= 12; i++) {
				Line L = new Line();
				grid.Children.Add(L);
				smallLines1.Add(L);
			}

			for(int i = 1; i <= 4; i++) {
				Line L = new Line();
				grid.Children.Add(L);
				bigLines1.Add(L);
			}

			drowClock();
		}
		public void changeColor(SolidColorBrush newBrush) {
			vBrush = newBrush;
		}
		public void drowClock(double x1new = 10, double y1new = 10, double vRadnew = 180) {
			x1 = x1new;
			y1 = y1new;
			vRad = vRadnew;

			grid.Width = vRad * 2; grid.Height = vRad * 2;
			grid.VerticalAlignment = VerticalAlignment.Top; grid.HorizontalAlignment = HorizontalAlignment.Left;
			grid.Margin = new Thickness(x1, y1, 0, 0);

			bigEl1.Width = vRad * 2; bigEl1.Height = vRad * 2;
			bigEl1.VerticalAlignment = VerticalAlignment.Top; bigEl1.HorizontalAlignment = HorizontalAlignment.Left;
			bigEl1.Stroke = vBrush; bigEl1.StrokeThickness = 5;
			bigEl1.Margin = new Thickness(0, 0, 0, 0);

			smallEl1.Width = vRad * 0.2; smallEl1.Height = vRad * 0.2;
			smallEl1.VerticalAlignment = VerticalAlignment.Center; smallEl1.HorizontalAlignment = HorizontalAlignment.Center;
			smallEl1.Stroke = vBrush; smallEl1.StrokeThickness = 5;
			smallEl1.Margin = new Thickness(0, 0, 0, 0);

			RotateTransform RT; int ang = 0;
			foreach(var i in smallLines1) {
				i.X1 = vRad; i.X2 = vRad;
				i.Y1 = vRad * 0.03; i.Y2 = vRad * 0.09;
				i.StrokeThickness = 5; i.Stroke = vBrush;

				RT = new RotateTransform(ang);
				RT.CenterX = vRad; RT.CenterY = vRad;
				i.RenderTransform = RT;

				ang += 30;
			}

			ang = 0;
			foreach(var i in bigLines1) {
				i.X1 = vRad; i.X2 = vRad;
				i.Y1 = vRad * 0.03; i.Y2 = vRad * 0.18;
				i.StrokeThickness = 5; i.Stroke = vBrush;

				RT = new RotateTransform(ang);
				RT.CenterX = vRad; RT.CenterY = vRad;
				i.RenderTransform = RT;

				ang += 90;
			}

			secondsL1.X1 = vRad; secondsL1.X2 = vRad;
			secondsL1.Y1 = vRad * 0.24; secondsL1.Y2 = vRad * 0.88;
			secondsL1.StrokeThickness = 5; secondsL1.Stroke = vBrush;

			minetsL1.X1 = vRad; minetsL1.X2 = vRad;
			minetsL1.Y1 = vRad * 0.44; minetsL1.Y2 = vRad * 0.88;
			minetsL1.StrokeThickness = 5; minetsL1.Stroke = vBrush;

			hoursL1.X1 = vRad; hoursL1.X2 = vRad;
			hoursL1.Y1 = vRad * 0.64; hoursL1.Y2 = vRad * 0.88;
			hoursL1.StrokeThickness = 5; hoursL1.Stroke = vBrush;
		}
		public void arrowMove(double angS, double angM, double angH) {
			RotateTransform Rs = new RotateTransform(angS);
			Rs.CenterX = vRad; Rs.CenterY = vRad;
			secondsL1.RenderTransform = Rs;

			RotateTransform Rm = new RotateTransform(angM);
			Rm.CenterX = vRad; Rm.CenterY = vRad;
			minetsL1.RenderTransform = Rm;

			RotateTransform Rh = new RotateTransform(angH);
			Rh.CenterX = vRad; Rh.CenterY = vRad;
			hoursL1.RenderTransform = Rh;
		}
	}
}
