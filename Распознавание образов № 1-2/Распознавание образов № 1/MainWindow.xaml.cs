﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Распознавание_образов___1 {
	public partial class MainWindow : Window {
		DispatcherTimer clockT1 = new DispatcherTimer(), clockT2 = new DispatcherTimer();

		cClock clock1, clock2;
		SolidColorBrush Brush = Brushes.Black;

		double x1 = 10, y1 = 10;
		double x2 = 10, y2 = 10;
		double vRad = 120, speed = 10;
		double center;

		bool autoMove = false;
		bool napr = true;

		public MainWindow() {
			InitializeComponent();

			radT.Text = vRad.ToString();
			ampT.Text = "100";
			perT.Text = "5";
			y1 = center = grid.Height / 2 - vRad;

			grid2 = new Grid();
			grid.Children.Add(grid2);
			clock2 = new cClock(grid2);
			clock2.changeColor(Brush);

			grid1 = new Grid();
			grid.Children.Add(grid1);
			clock1 = new cClock(grid1);

			moveClock();

			clockT1.Interval = TimeSpan.FromMilliseconds(100);
			clockT1.Tick += clock_Tick;
			clockT1.Start();

			clockT2.Interval = TimeSpan.FromMilliseconds(10);
			clockT2.Tick += clock_Tick1;
			clockT2.Start();
		}
		void clock_Tick (object sender, EventArgs e) {
			double milsec = DateTime.Now.Millisecond;
			double    sec = DateTime.Now.Second;
			double    min = DateTime.Now.Minute;
			double     hr = DateTime.Now.Hour;

			double angS = ((sec / 60) * 360) + ((milsec / 1000) * 6);
			double angM = (min * 360 / 60) + ((sec / 60) * 6);
            double angH = (hr * 360 / 12) + (min / 2);

			clock1.arrowMove(angS, angM, angH);
			clock2.arrowMove(angS, angM, angH);

			textBox1.Text = "основа\nx = " + x1.ToString() + 
							",\ny = " + y1.ToString() +
							"\n\nдоп.\nx = " + x2.ToString() + 
							",\ny = " + y2.ToString();
		}
		void clock_Tick1(object sender, EventArgs e) {
			if(autoMove) {
				double dX=0, dY=0;
				Double.TryParse(ampT.Text, out dY);
				Double.TryParse(perT.Text, out dX);
				
				x1 += dX;
				if (napr)	y1 += 5;
				else		y1 -= 5;

				if((center - dY) > y1)
					napr = !napr;
				if((center + dY) < y1)
					napr = !napr;

				moveClock();
			}
		}
		void moveClock() {
			double xMin = 0;
			double xMax = grid.ActualWidth;
			double yMin = 0;
			double yMax = grid.ActualHeight;

			bool colorBrush = false;

			x2 = x1; y2 = y1;

			if ((x1 + vRad*2)>xMax){
				x2 = x1 - xMax;
				colorBrush = true;
			}
			if((y1 + vRad * 2) > yMax) {
				y2 = y1 - yMax;
				colorBrush = true;
			}
			if(x1 < xMin) {
				x2 = x1 + xMax;
				colorBrush = true;
			}
			if(y1 < yMin) {
				y2 = y1 + yMax;
				colorBrush = true;
			}

			if(x1 > xMax) {
				x1 = x2;
				colorBrush = false;
			}
			if(y1 > yMax) {
				y1 = y2;
				colorBrush = false;
			}
			if(x1 < (xMin - 2 * vRad)) {
				x1 = x2;
				colorBrush = false;
			}
			if(y1 < (yMin - 2 * vRad)) {
				y1 = y2;
				colorBrush = false;
			}

			if(colorBrush)
				Brush = Brushes.White;
			else
				Brush = Brushes.Black;

			
			clock1.drowClock(x1, y1, vRad);
			clock2.changeColor(Brush);
			clock2.drowClock(x2, y2, vRad);
		}
		private void Window_KeyDown(object sender, KeyEventArgs e) {
			switch(e.Key) {
				case Key.Right:
					x1 += speed;
					break;
				case Key.Left:
					x1 -= speed;
					break;
				case Key.Up:
					y1 -= speed;
					break;
				case Key.Down:
					y1 += speed;
					break;
			}
			moveClock();
		}
		private void CheckBox_Checked(object sender, RoutedEventArgs e) {
			y1 = center = grid.Height / 2 - vRad;
			autoMove = CheckBox1.IsChecked.Value;
		}
		private void CheckBox1_Unchecked(object sender, RoutedEventArgs e) {
			autoMove = CheckBox1.IsChecked.Value;
		}
		private void sButP_Click(object sender, RoutedEventArgs e) {
			if(vRad < 800) vRad += 10;
			radT.Text = vRad.ToString();
			moveClock();
		}
		private void sButM_Click(object sender, RoutedEventArgs e) {
			if(vRad > 0) vRad -= 10;
			radT.Text = vRad.ToString();
			moveClock();
		}
		private void aButP_Click(object sender, RoutedEventArgs e) {
			double dY = 0;
			if(!Double.TryParse(ampT.Text, out dY)) ampT.Text = "0";
			if(dY < 500) dY += 10;
			ampT.Text = dY.ToString();
		}
		private void aButM_Click(object sender, RoutedEventArgs e) {
			double dY = 0;
			if(!Double.TryParse(ampT.Text, out dY)) ampT.Text = "0";
			if(dY > 0) dY -= 10;
			ampT.Text = dY.ToString();
		}
		private void pButP_Click(object sender, RoutedEventArgs e) {
			double dX = 0;
			if(!Double.TryParse(perT.Text, out dX)) ampT.Text = "0";
			if(dX < 20) dX += 1;
			perT.Text = dX.ToString();
		}
		private void pButM_Click(object sender, RoutedEventArgs e) {
			double dX = 0;
			if(!Double.TryParse(perT.Text, out dX)) ampT.Text = "0";
			if(dX > 0) dX -= 1;
			perT.Text = dX.ToString();
		}
	}	

}