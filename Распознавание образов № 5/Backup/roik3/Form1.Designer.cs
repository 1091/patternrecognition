﻿namespace roik3
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.panel1 = new System.Windows.Forms.Panel();
			this.button4 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.tabControl2 = new System.Windows.Forms.TabControl();
			this.button1 = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.numSize = new System.Windows.Forms.NumericUpDown();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.label2 = new System.Windows.Forms.Label();
			this.numVecSize = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.numClassCount = new System.Windows.Forms.NumericUpDown();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.tableLayoutPanel1.SuspendLayout();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numSize)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numVecSize)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numClassCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.3035F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 53.6965F));
			this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 1, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 1;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(771, 610);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.checkBox1);
			this.panel1.Controls.Add(this.button4);
			this.panel1.Controls.Add(this.button3);
			this.panel1.Controls.Add(this.button2);
			this.panel1.Controls.Add(this.tabControl2);
			this.panel1.Controls.Add(this.button1);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.numSize);
			this.panel1.Controls.Add(this.tabControl1);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.numVecSize);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.numClassCount);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(3, 3);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(350, 604);
			this.panel1.TabIndex = 7;
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(174, 38);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(92, 23);
			this.button4.TabIndex = 18;
			this.button4.Text = "Координаты";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Click += new System.EventHandler(this.numClassCount_ValueChanged);
			// 
			// button3
			// 
			this.button3.Enabled = false;
			this.button3.Location = new System.Drawing.Point(174, 70);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(125, 23);
			this.button3.TabIndex = 17;
			this.button3.Text = "Метод Байеса";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// button2
			// 
			this.button2.Enabled = false;
			this.button2.Location = new System.Drawing.Point(12, 70);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(146, 23);
			this.button2.TabIndex = 16;
			this.button2.Text = "Метод мин. расстояний";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// tabControl2
			// 
			this.tabControl2.Location = new System.Drawing.Point(12, 361);
			this.tabControl2.Name = "tabControl2";
			this.tabControl2.SelectedIndex = 0;
			this.tabControl2.Size = new System.Drawing.Size(338, 234);
			this.tabControl2.TabIndex = 15;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(272, 38);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 8;
			this.button1.Text = "Генерация";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(9, 43);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(89, 13);
			this.label3.TabIndex = 14;
			this.label3.Text = "Объем выборки";
			// 
			// numSize
			// 
			this.numSize.Increment = new decimal(new int[] {
            50,
            0,
            0,
            0});
			this.numSize.Location = new System.Drawing.Point(104, 41);
			this.numSize.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numSize.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
			this.numSize.Name = "numSize";
			this.numSize.Size = new System.Drawing.Size(54, 20);
			this.numSize.TabIndex = 13;
			this.numSize.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
			// 
			// tabControl1
			// 
			this.tabControl1.Location = new System.Drawing.Point(12, 121);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(338, 234);
			this.tabControl1.TabIndex = 12;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(171, 6);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(119, 13);
			this.label2.TabIndex = 11;
			this.label2.Text = "Размерность вектора";
			// 
			// numVecSize
			// 
			this.numVecSize.Location = new System.Drawing.Point(296, 5);
			this.numVecSize.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.numVecSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numVecSize.Name = "numVecSize";
			this.numVecSize.Size = new System.Drawing.Size(54, 20);
			this.numVecSize.TabIndex = 10;
			this.numVecSize.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
			this.numVecSize.ValueChanged += new System.EventHandler(this.numClassCount_ValueChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(9, 6);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(84, 13);
			this.label1.TabIndex = 9;
			this.label1.Text = "Число классов";
			// 
			// numClassCount
			// 
			this.numClassCount.Location = new System.Drawing.Point(104, 4);
			this.numClassCount.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.numClassCount.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
			this.numClassCount.Name = "numClassCount";
			this.numClassCount.Size = new System.Drawing.Size(54, 20);
			this.numClassCount.TabIndex = 7;
			this.numClassCount.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
			this.numClassCount.ValueChanged += new System.EventHandler(this.numClassCount_ValueChanged);
			// 
			// pictureBox1
			// 
			this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pictureBox1.Location = new System.Drawing.Point(359, 3);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(409, 604);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox1.TabIndex = 8;
			this.pictureBox1.TabStop = false;
			// 
			// checkBox1
			// 
			this.checkBox1.AutoSize = true;
			this.checkBox1.Checked = true;
			this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBox1.Location = new System.Drawing.Point(19, 99);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(64, 17);
			this.checkBox1.TabIndex = 19;
			this.checkBox1.Text = "Оценки";
			this.checkBox1.UseVisualStyleBackColor = true;
			// 
			// Form1
			// 
			this.AcceptButton = this.button1;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(771, 610);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.tableLayoutPanel1.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numSize)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numVecSize)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numClassCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.NumericUpDown numSize;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.NumericUpDown numVecSize;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.NumericUpDown numClassCount;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.TabControl tabControl2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.CheckBox checkBox1;
	}
}

